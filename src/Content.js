// import { DatePicker } from 'antd';
import { Typography, Row, Col, Avatar } from "antd";
import { UserOutlined } from '@ant-design/icons';

const { Title, Text } = Typography;
function Content() {
  return (
    <div>
      <Row justify="center">
        <Col span={8}>
          <Title level={3}>Details</Title>
          <Title level={5}>Name</Title>
          <Text>Tubagus Mochammad Eza</Text>

          <Title level={5}>Age</Title>
          <Text>21 Years</Text>

          <Title level={5}>Location</Title>
          <Text>Malang, East Java, Indonesia</Text>
        </Col>
        <Col span={8}>
          <Title level={3}>About Me</Title>
          <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Text>
        </Col>
        <Col span={8}>
          <Avatar size={200} icon={<UserOutlined />} style={{verticalAlign: ''}}/>
        </Col>
      </Row>
    </div>



  );
}

export default Content;
