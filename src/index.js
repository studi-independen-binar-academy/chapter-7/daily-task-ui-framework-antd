import React from "react";
import ReactDOM from "react-dom/client";
import Module from "./Module";
import Styled from "./Styled";
import "./index.css";
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import App from './App';
import Content from "./Content";
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <main>
      <App />
      <Content />
    </main>

    <Module />

    <Styled />
  </React.StrictMode>
);
