// import { DatePicker } from 'antd';
import { Typography, Row, Col } from "antd";

const { Title } = Typography;
function App() {
  return (
    <div>
      <Row>
        <Col span={24}>
          <div className='App'>
            <Title style={{ color: 'darkslateblue' }}>Hello World!</Title>
            <Title level={5}>I'm a Fullstack Web Developer</Title>
          </div>
        </Col>
      </Row>
    </div>



  );
}

export default App;
